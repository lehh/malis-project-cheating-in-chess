#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This code modifies organic games, into cheating games by a synthetic process. 
"""

import csv
import random

# Read in the original csv file
with open('7data_600+0_White_Win_2.csv', 'r') as csv_file:
    reader = csv.DictReader(csv_file)
    rows = list(reader)

# Keep track of unique game numbers
game_numbers = set()
for row in rows:
    game_numbers.add(row['Game number'])

# Assign random "Cheating Percentage Aim" values for each unique game number
cheating_percentages = {}
for game_number in game_numbers:
    if random.uniform(0, 1) <= 0.5:
        cheating_percentages[game_number] = 0
    else:
        cheating_percentages[game_number] = round(random.uniform(0.5, 1), 2)

# Add "Cheating", "Cheating Percentage Aim", and "Cheating Percentage Realised" columns
for row in rows:
    game_number = row['Game number']
    row['Cheating Percentage Aim'] = cheating_percentages[game_number]
    if cheating_percentages[game_number] == 0:
        row['Cheating'] = 'No'
    else:
        if random.uniform(0, 1) <= cheating_percentages[game_number]:
            row['Cheating'] = 'Yes'
        else:
            row['Cheating'] = 'No'
    row['Cheating Percentage Realised'] = 0

# Count the number of times "Cheating" is "Yes" for each unique game number
cheating_counts = {}
for game_number in game_numbers:
    cheating_counts[game_number] = 0
for row in rows:
    game_number = row['Game number']
    if row['Cheating'] == 'Yes':
        cheating_counts[game_number] += 1

# Calculate "Cheating Percentage Realised" for each unique game number
for row in rows:
    game_number = row['Game number']
    total_count = len([r for r in rows if r['Game number'] == game_number])
    if cheating_percentages[game_number] != 0:
        row['Cheating Percentage Realised'] = round(cheating_counts[game_number] / total_count, 2)
        
# Change the accuracy to 1 if cheating is done in this move
for row in rows:
    if row['Cheating'] == 'Yes':
        row['Accuracy'] = 100

# Write the modified rows to a new csv file
with open('data_with_cheating.csv', 'w') as csv_file:
    writer = csv.DictWriter(csv_file, fieldnames=rows[0].keys())
    writer.writeheader()
    for row in rows:
        writer.writerow(row)
        
        

