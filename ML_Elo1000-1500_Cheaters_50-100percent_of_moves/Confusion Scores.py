#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Printing different scores from the confusion matrix. The numbers have to plotted manually in the beginning. 
"""



"""
{'3': 242, 
 '2': 142,
 '1': 111,
 '4': 11}
"""


"""
false_negative_label = 1
true_positive_label = 2
true_negative_label = 3
false_positive_label = 4
"""

# Confusion matrix
confusion_matrix = {
    "False negative": 111,
    "True positive": 142,
    "True negative": 242,
    "False positive": 11
}

# Accuracy
accuracy = (confusion_matrix["True positive"] + confusion_matrix["True negative"]) / sum(confusion_matrix.values())
print("Accuracy: ", accuracy)

# Precision
precision = confusion_matrix["True positive"] / (confusion_matrix["True positive"] + confusion_matrix["False positive"])
print("Precision: ", precision)

# Recall (Sensitivity or True Positive Rate)
recall = confusion_matrix["True positive"] / (confusion_matrix["True positive"] + confusion_matrix["False negative"])
print("Recall: ", recall)

# Specificity (True Negative Rate)
specificity = confusion_matrix["True negative"] / (confusion_matrix["True negative"] + confusion_matrix["False positive"])
print("Specificity: ", specificity)

# F1 Score
f1_score = 2 * ((precision * recall) / (precision + recall))
print("F1 Score: ", f1_score)

# Matthew's correlation coefficient
mcc = ((confusion_matrix["True positive"] * confusion_matrix["True negative"]) - (confusion_matrix["False positive"] * confusion_matrix["False negative"])) / (((confusion_matrix["True positive"] + confusion_matrix["False positive"]) * (confusion_matrix["True positive"] + confusion_matrix["False negative"]) * (confusion_matrix["True negative"] + confusion_matrix["False positive"]) * (confusion_matrix["True negative"] + confusion_matrix["False negative"])) ** (1/2))
print("Matthew's correlation coefficient: ", mcc)
