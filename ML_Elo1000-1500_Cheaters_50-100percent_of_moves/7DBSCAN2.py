#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
DBSCAN, training and test. Also finding confusion results. 
"""

from sklearn.cluster import DBSCAN
from sklearn.preprocessing import StandardScaler
import pandas as pd
import matplotlib.pyplot as plt
import csv
import shutil



"""
function to perform DBSCAN
"""
global_eps= 0.4
global_min_samples = 10



def dbscan_clustering(file_path, eps = global_eps, min_samples = global_min_samples, plot = True):
    # Read the data from the csv file
    data = pd.read_csv(file_path)
    
    # Select the columns to use for clustering
    X = data[["Top Ratio", "Bottom Ratio"]]
    
    # Scale the data
    scaler = StandardScaler()
    X_scaled = scaler.fit_transform(X)
    
    # Perform DBSCAN clustering
    dbscan = DBSCAN(eps=eps, min_samples=min_samples)
    clusters = dbscan.fit_predict(X_scaled)
    
    # Add the cluster labels to the data
    data["Cluster"] = clusters
    
    # Save the clustered data to a new csv file
    data.to_csv("clustered_"+file_path, index=False)
    if plot:
        # Create a scatter plot of the data, colored by cluster label
        scatter = plt.scatter(data["Top Ratio"], data["Bottom Ratio"], c=data["Cluster"], cmap="rainbow", alpha=0.3)
    
        # Add a legend
        legend = plt.legend(*scatter.legend_elements(),title = 'Cluster label')
    
        # Add a title
        #plt.title("DBSCAN Clustering")
    
        # Add axis labels
        plt.xlabel("Weak Ratio")
        plt.ylabel("Strong Ratio")
    
        # Show the plot
        plt.show()
    else:
        pass




    

dbscan_clustering("region_counts3.csv")






"""
Extract the inlier points from the DBSCAN training
"""
# Read the data from the csv file
data = pd.read_csv("clustered_region_counts3.csv")

# Extract all rows that has Cluster = 0
inlier_points = data[data["Cluster"] >= 0]

# Save the extracted rows to a new csv file
inlier_points.to_csv("inlier_points_training.csv", index=False)




"""
Reformat the "Cluster" row
"""
# Open the original CSV file for reading
with open("inlier_points_training.csv", "r") as in_file:
    # Create a new file for writing the modified data
    with open("inlier_points_training_modified.csv", "w", newline="") as out_file:
        # Use the csv module to read and write the data
        reader = csv.reader(in_file)
        writer = csv.writer(out_file)

        # Get the header row from the original file
        headers = next(reader)

        # Change the header name from "Cluster" to "Model/Test"
        headers[headers.index("Cluster")] = "Model/Test"

        # Write the modified header row to the new file
        writer.writerow(headers)

        # Iterate through the rest of the rows
        for row in reader:
            # Change the value in the "Cluster" column to "Model"
            row[headers.index("Model/Test")] = "Model"

            # Write the modified row to the new file
            writer.writerow(row)










"""
Creating a copy of "inlier_points_training_modified.csv" and calling it "temporary_DBSCAN_points.csv". Also creating an empty CSV called "classified_data_with_cheating.csv". 

Doing the following procedure to be done for all rows in  labeled_region_counts2.csv, one by one, in the following order.

1. concatinate the current row to the end of "temporary.csv"
2. Perform the previously defined function dbscan_clustering(temporary_DBSCAN_points.csv).
3. Taking the last row of "clustered_temporary_DBSCAN_points.csv" and put it into "classified_data_with_cheating.csv"
4. Deleting the last row of "temporary.csv"
"""


# Create a copy of "inlier_points_training_modified.csv" and call it "temporary_DBSCAN_points.csv"
shutil.copyfile("inlier_points_training_modified.csv", "temporary_DBSCAN_points.csv")

# Create an empty CSV called "classified_data_with_cheating.csv"
with open("classified_data_with_cheating.csv", "w") as classified_file:
    writer = csv.writer(classified_file)
    writer.writerow([])

# Open "inlier_points_training_modified.csv" for reading
with open("labeled_region_counts2.csv", "r") as labeled_file:
    reader = csv.reader(labeled_file)
    next(reader) # skip the header row
    for row in reader:
        # Append the current row to the end of "temporary_DBSCAN_points.csv"
        with open("temporary_DBSCAN_points.csv", "a") as temp_file:
            writer = csv.writer(temp_file)
            writer.writerow(row)

        # Perform the dbscan_clustering function
        dbscan_clustering("temporary_DBSCAN_points.csv", plot=False)

        # Take the last row of "clustered_temporary_DBSCAN_points.csv" and put it into "classified_data_with_cheating.csv"
        with open("clustered_temporary_DBSCAN_points.csv", "r") as clustered_file:
            lines = clustered_file.readlines()
            last_line = lines[-1]
            with open("classified_data_with_cheating.csv", "a") as classified_file:
                classified_file.write(last_line)

        # Delete the last row of "temporary_DBSCAN_points.csv"
        with open("temporary_DBSCAN_points.csv", "r") as temp_file:
            lines = temp_file.readlines()
            lines = lines[:-1]
        with open("temporary_DBSCAN_points.csv", "w") as temp_file:
            temp_file.writelines(lines)





# Open "temporary_DBSCAN_points.csv" to get the header
with open("clustered_temporary_DBSCAN_points.csv", "r") as temp_file:
    reader = csv.reader(temp_file)
    header = next(reader) # get the header

# Open "classified_data_with_cheating.csv" and insert the header at the top of the file
with open("classified_data_with_cheating.csv", "r+") as classified_file:
    content = classified_file.read()
    classified_file.seek(0, 0)
    # writer = csv.writer(classified_file)
    # writer.writerow(header)
    classified_file.write(",".join(header) + "\n" + content)




"""
Add cunfusion labels to games
"""

false_negative_label = 1
true_positive_label = 2
true_negative_label = 3
false_positive_label = 4



# Read the data from the csv file
data = pd.read_csv("classified_data_with_cheating.csv")

# Add a new column "Confusion"
data["Confusion"] = ""

# Iterate through the rows in the dataframe
for index, row in data.iterrows():
    # Get the cheating percentage and cluster value
    cheating_percentage = row["Cheating Percentage Realised"]
    cluster_value = row["Cluster"]
    # Check if the row is a false negative
    if cheating_percentage > 0 and cluster_value >= 0:
        data.at[index, "Confusion"] = false_negative_label
    # Check if the row is a true positive
    elif cheating_percentage > 0 and cluster_value < 0:
        data.at[index, "Confusion"] = true_positive_label
    # Check if the row is a true negative
    elif cheating_percentage == 0 and cluster_value >= 0:
        data.at[index, "Confusion"] = true_negative_label
    # Check if the row is a false positive
    elif cheating_percentage == 0 and cluster_value < 0:
        data.at[index, "Confusion"] = false_positive_label

# Save the dataframe to a new csv file
data.to_csv("classified_data_with_cheating_confusion.csv", index=False)





"""
count the occurrences of each value in the "Confusion" column
"""
# Create a dictionary to store the count of each value in the "Confusion" column
counts = {}

# Open the CSV file for reading
with open("classified_data_with_cheating_confusion.csv", "r") as csv_file:
    reader = csv.DictReader(csv_file)
    # Iterate through the rows in the CSV file
    for row in reader:
        # Get the value in the "Confusion" column
        confusion = row["Confusion"]
        # Check if the value is already in the counts dictionary
        if confusion in counts:
            # Increment the count for that value
            counts[confusion] += 1
        else:
            # Initialize the count for that value
            counts[confusion] = 1

# Print the count of each value in the "Confusion" column
print(counts)







"""
Plotting cheating results
"""


# Read the data from the csv file
data = pd.read_csv("classified_data_with_cheating_confusion.csv")

# Create a scatter plot of the data, colored by Confusion column
scatter = plt.scatter(data["Top Ratio"], data["Bottom Ratio"], c=data["Confusion"], cmap="rainbow", alpha=0.3)

# Add a legend
legend = plt.legend(*scatter.legend_elements(),title = 'Confusion')

# Add a title
#plt.title("DBSCAN Test Results")

# Add axis labels
plt.xlabel("Strong Ratio")
plt.ylabel("Weak Ratio")

# Show the plot
plt.show()














