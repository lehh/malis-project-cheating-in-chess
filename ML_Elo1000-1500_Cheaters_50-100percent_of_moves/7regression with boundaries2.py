#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This modifies organic games, into cheating games by a synthetic process. 
"""

import csv
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures



"""
Performing polynomial regression on "7data_600+0_White_Win_1.csv" and creating two new dataframes (above_df and below_df) based on accuracy values. Plotting data points in above_df and below_df as separate scatter plots and polynomial regression curve for the entire dataset.
"""

# load data
df = pd.read_csv("7data_600+0_White_Win_1.csv")

# create polynomial features
poly = PolynomialFeatures(degree=2)
X_poly = poly.fit_transform(df[['Decesion time']])

# create and fit the linear regression model
reg = LinearRegression().fit(X_poly, df['Accuracy'])

# Get the predicted values for the points in the dataset
y_pred = reg.predict(X_poly)

# Create a new column in the dataframe with the predicted values
df['Predicted Accuracy'] = y_pred

# Create a dataframe for points above the regression line
above_df = df[df['Accuracy'] > df['Predicted Accuracy']]

# Create a dataframe for points below the regression line
below_df = df[df['Accuracy'] < df['Predicted Accuracy']]

# create scatter plot of data
plt.scatter(above_df['Decesion time'], above_df['Accuracy'], color ='purple', alpha=0.1)
plt.scatter(below_df['Decesion time'], below_df['Accuracy'], alpha=0.1)

# plot the polynomial regression curve
xx = np.linspace(df['Decesion time'].min(), df['Decesion time'].max(), 100)
yy = reg.predict(poly.transform(xx.reshape(xx.shape[0], 1)))
plt.plot(xx, yy, color='green')










"""
Creating two polynomial regression curves for the above_df and below_df datasets and then plotting the area between the two curves.
"""

# create polynomial features
X_poly_above = poly.fit_transform(above_df[['Decesion time']])

# create and fit the linear regression model for above_df
reg_above = LinearRegression().fit(X_poly_above, above_df['Accuracy'])

# plot the polynomial regression curve for above_df
xx_above = np.linspace(above_df['Decesion time'].min(), above_df['Decesion time'].max(), 100)
yy_above = reg_above.predict(poly.transform(xx_above.reshape(xx_above.shape[0], 1)))
#plt.plot(xx_above, yy_above, color='red')

# create polynomial features
X_poly_below = poly.fit_transform(below_df[['Decesion time']])

# create and fit the linear regression model for below_df
reg_below = LinearRegression().fit(X_poly_below, below_df['Accuracy'])

# plot the polynomial regression curve for below_df
xx_below = np.linspace(below_df['Decesion time'].min(), below_df['Decesion time'].max(), 100)
yy_below = reg_below.predict(poly.transform(xx_below.reshape(xx_below.shape[0], 1)))
#plt.plot(xx_below, yy_below, color='red')

# Define x-coordinates and y-coordinates for the two lines
x = np.linspace(df['Decesion time'].min(), df['Decesion time'].max(), 100)
y1 = yy_above
y2 = yy_below

# Fill the region between the two lines with gray color
plt.fill_between(x, y1, y2, color='green', alpha=0.2)

# add labels and show the plot
plt.xlabel("Decision Time")
plt.ylabel("Accuracy")
plt.show()







"""
Using polynomial regression models trained on "7data_600+0_White_Win_1.csv" to make predictions on "7data_600+0_White_Win_3.csv" and adding columns showing the relation to the regression curves.
"""

# load test data
test_df = pd.read_csv("7data_600+0_White_Win_3.csv")

# create polynomial features for test data
test_X_poly = poly.fit_transform(test_df[['Decesion time']])

# predict accuracy for test data using reg
test_y_pred_reg = reg.predict(test_X_poly)

# Create a new column in the test dataframe with the predicted values
test_df['Predicted Accuracy'] = test_y_pred_reg

# Create a new column in the test dataframe indicating which regression line the test point is above
test_df['Above Line'] = np.where(test_df['Accuracy'] > test_df['Predicted Accuracy'], 'Above', 'Below')

# write the test dataframe to a csv file
test_df.to_csv('test_results3.csv', index=False)

# predict accuracy for test data using reg_above
test_y_pred_above = reg_above.predict(test_X_poly)

# Create a new column in the test dataframe with the predicted values using reg_above
test_df['Predicted Accuracy Above'] = test_y_pred_above

# Create a new column in the test dataframe indicating whether the test point is above or below the regression line using reg_above
test_df['Above Line Above'] = np.where(test_df['Accuracy'] > test_df['Predicted Accuracy Above'], 'Above', 'Below')

# predict accuracy for test data using reg_below
test_y_pred_below = reg_below.predict(test_X_poly)

# Create a new column in the test dataframe with the predicted values using reg_below
test_df['Predicted Accuracy Below'] = test_y_pred_below

# Create a new column in the test dataframe indicating whether the test point is above or below the regression line using reg_below
test_df['Above Line Below'] = np.where(test_df['Accuracy'] > test_df['Predicted Accuracy Below'], 'Above', 'Below')

# write the test dataframe to a csv file
test_df.to_csv('test_results3.csv', index=False)






"""
Doing the last step for 'data_with_cheating.csv' as well. This data will not be used anymore in this file.
"""

# load test data for first file
test_df2 = pd.read_csv("data_with_cheating.csv")

# create polynomial features for test data
test_X_poly2 = poly.fit_transform(test_df2[['Decesion time']])

# predict accuracy for test data using reg
test_y_pred_reg2 = reg.predict(test_X_poly2)

# Create a new column in the test dataframe with the predicted values
test_df2['Predicted Accuracy'] = test_y_pred_reg2

# Create a new column in the test dataframe indicating which regression line the test point is above
test_df2['Above Line'] = np.where(test_df2['Accuracy'] > test_df2['Predicted Accuracy'], 'Above', 'Below')

# write the test dataframe to a csv file
test_df2.to_csv('test_results2.csv', index=False)

# predict accuracy for test data using reg_above
test_y_pred_above2 = reg_above.predict(test_X_poly2)

# Create a new column in the test dataframe with the predicted values using reg_above
test_df2['Predicted Accuracy Above'] = test_y_pred_above2

# Create a new column in the test dataframe indicating whether the test point is above or below the regression line using reg_above
test_df2['Above Line Above'] = np.where(test_df2['Accuracy'] > test_df2['Predicted Accuracy Above'], 'Above', 'Below')

# predict accuracy for test data using reg_below
test_y_pred_below2 = reg_below.predict(test_X_poly2)

# Create a new column in the test dataframe with the predicted values using reg_below
test_df2['Predicted Accuracy Below'] = test_y_pred_below2

# Create a new column in the test dataframe indicating whether the test point is above or below the regression line using reg_below
test_df2['Above Line Below'] = np.where(test_df2['Accuracy'] > test_df2['Predicted Accuracy Below'], 'Above', 'Below')

# write the test dataframe to a csv file
test_df2.to_csv('test_results2.csv', index=False)










"""
Reads "test_results"...".csv". Creates a new file "region_test_results"...".csv" which is the same with the the additional column, "Region".
"""

def add_region(input_file, output_file):
    # Open the original CSV file
    with open(input_file, "r") as original_file:
        # Create a new CSV file to write the reformatted data to
        with open(output_file, "w", newline="") as reformatted_file:
            # Create a CSV reader and writer
            reader = csv.reader(original_file)
            writer = csv.writer(reformatted_file)

            # Write the header row to the new file
            headers = next(reader)
            headers.append("Region")
            writer.writerow(headers)

            # Find the index of the column you want to use
            column_index1 = headers.index("Above Line Above")
            column_index2 = headers.index("Above Line Below")
            

            # Iterate through the rows in the original file
            for row in reader:
                # Determine the value for the "Region" column
                if row[column_index1] == "Above":
                    region = "top"
                elif row[column_index2] == "Below":
                    region = "bottom"
                else:
                    region = "middle"

                # Add the "Region" value to the row
                row.append(region)

                # Write the row to the new file
                writer.writerow(row)


add_region("test_results3.csv", "region_test_results3.csv")
add_region("test_results2.csv", "region_test_results2.csv")


#print(data.columns)
#print(data.dtypes)






"""
Plotting 'region_test_results2.csv'
"""

# Read the csv file
df = pd.read_csv('region_test_results2.csv')

# Create a new column 'color' based on the value of the 'Region' column
df['color'] = df.apply(lambda row: 'red' if row['Region'] == 'top' else ('green' if row['Region'] == 'middle' else 'blue'), axis=1)

# Plot the Accuracy column against the Decision Time column, using the 'color' column to distinguish the points
plt.scatter(df['Decesion time'], df['Accuracy'], c=df['color'], alpha=0.3)
plt.xlabel('Decision Time')
plt.ylabel('Accuracy')
plt.show()

























