#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This part extracts the data about white players that end up winning the game

"""


import pandas as pd
import csv
from collections import defaultdict

# Constants for the input and output file names
INPUT_FILE = "50data_600+0.csv"
OUTPUT_FILE = "7data_600+0_White_Win.csv"

# Constants for the filter criteria
GAME_RESULT = 1
WHITE_BLACK = "White"
ELO_MIN = 1000
ELO_MAX = 1500

# Open the input file and read all the rows
with open(INPUT_FILE, "r") as input_file:
    rows = list(csv.reader(input_file))

# Remove the header row
header = rows.pop(0)

# Filter the rows based on the criteria
filtered_rows = [row for row in rows if float(row[5]) == GAME_RESULT and row[2] == WHITE_BLACK and ELO_MIN <= int(row[4]) <= ELO_MAX]

# Open the output file and write the filtered rows
with open(OUTPUT_FILE, "w") as output_file:
    csv_writer = csv.writer(output_file)
    csv_writer.writerow(header)
    csv_writer.writerows(filtered_rows)





"""
This part seperates the the the data into three csv files

"""



# Constants for the input and output file names
INPUT_FILE = "7data_600+0_White_Win.csv"
OUTPUT_FILE_1 = "7data_600+0_White_Win_1.csv"
OUTPUT_FILE_2 = "7data_600+0_White_Win_2.csv"
OUTPUT_FILE_3 = "7data_600+0_White_Win_3.csv"

# Constants for the percentage split
SPLIT_PERCENTAGE_1 = 0.2
SPLIT_PERCENTAGE_2 = 0.2

# Open the input file and read all the rows
with open(INPUT_FILE, "r") as input_file:
    rows = list(csv.reader(input_file))

# Get the header
header = rows.pop(0)

# Create a dictionary to store the rows of each game number
game_number_rows = defaultdict(list)
for row in rows:
    game_number_rows[row[0]].append(row)

# Get the unique game numbers
game_numbers = list(game_number_rows.keys())

# Calculate the number of unique game numbers for each output file
num_game_numbers_1 = int(len(game_numbers) * SPLIT_PERCENTAGE_1)
num_game_numbers_2 = int(len(game_numbers) * SPLIT_PERCENTAGE_2)

# Split the unique game numbers into three lists
game_numbers_1 = game_numbers[:num_game_numbers_1]
game_numbers_2 = game_numbers[num_game_numbers_1:num_game_numbers_1+num_game_numbers_2]
game_numbers_3 = game_numbers[num_game_numbers_1+num_game_numbers_2:]

# Create three lists to store the rows for each output file
rows_1 = []
rows_2 = []
rows_3 = []

# Append the rows of each game number to the corresponding list
for game_number in game_numbers_1:
    rows_1.extend(game_number_rows[game_number])
for game_number in game_numbers_2:
    rows_2.extend(game_number_rows[game_number])
for game_number in game_numbers_3:
    rows_3.extend(game_number_rows[game_number])

# Open the output files and write the rows
with open(OUTPUT_FILE_1, "w") as output_file_1:
    csv_writer = csv.writer(output_file_1)
    csv_writer.writerow(header)
    csv_writer.writerows(rows_1)
with open(OUTPUT_FILE_2, "w") as output_file_2:
    csv_writer = csv.writer(output_file_2)
    csv_writer.writerow(header)
    csv_writer.writerows(rows_2)
with open(OUTPUT_FILE_3, "w") as output_file_3:
    csv_writer = csv.writer(output_file_3)
    csv_writer.writerow(header)
    csv_writer.writerows(rows_3)
    
    
"""
Prints the number of rows in "7data_600+0_White_Win"
"""

# specify the file path
file_path = "7data_600+0_White_Win.csv"

# read the file into a pandas dataframe
df = pd.read_csv(file_path)

# find the number of rows in the dataframe
num_rows = df.shape[0]

# print the number of rows
print("Number of rows in the file:", num_rows)

