#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This modifies organic games, into cheating games by a synthetic process. 
"""



import csv
from collections import defaultdict




# Constants for the input and output file names
INPUT_FILE = "region_test_results2.csv"
OUTPUT_FILE = "region_counts2.csv"

# Open the input file and read all the rows
with open(INPUT_FILE, "r") as input_file:
    rows = list(csv.reader(input_file))

#Remove the header row
header = rows.pop(0)

# Create a dictionary to store the counts of rows for each unique game number in each region
region_counts = defaultdict(lambda: defaultdict(int))

# Create a dictionary to store the cheating percentages for each unique game number
cheating_percentages = defaultdict(float)

# Iterate through the rows and update the counts
for row in rows:
    game_number = row[0]
    region = row[-1]
    region_counts[game_number][region] += 1
    cheating_percentages[game_number] = float(row[10])

# Open the output file and write the counts
with open(OUTPUT_FILE, "w", newline='') as output_file:
    csv_writer = csv.writer(output_file)
    csv_writer.writerow(["Game Number", "Top", "Middle", "Bottom", "Sum", "Top Ratio", "Bottom Ratio", "Cheating Percentage Realised"])
    for game_number, counts in region_counts.items():
        sum_counts = sum(counts.values())
        top_ratio = counts["top"] / sum_counts
        bottom_ratio = counts["bottom"] / sum_counts
        csv_writer.writerow([game_number, counts["top"], counts["middle"], counts["bottom"], sum_counts, top_ratio, bottom_ratio, cheating_percentages[game_number]])







"""
Creating region_counts region counts
"""

import csv

# Open the input file in read mode
with open('region_counts2.csv', 'r') as in_file:
    # Create a CSV reader object
    reader = csv.reader(in_file)
    
    # Get the header row
    header = next(reader)
    
    # Add the new column "Test" to the header
    header.append("Test")
    
    # Open the output file in write mode
    with open('labeled_region_counts2.csv', 'w', newline='') as out_file:
        # Create a CSV writer object
        writer = csv.writer(out_file)
        
        # Write the header row to the output file
        writer.writerow(header)
        
        # Iterate over the rows in the input file
        for row in reader:
            # Append the value "Test" to the row
            row.append("Test")
            
            # Write the row to the output file
            writer.writerow(row)





# Constants for the input and output file names
INPUT_FILE = "region_test_results3.csv"
OUTPUT_FILE = "region_counts3.csv"

# Open the input file and read all the rows
with open(INPUT_FILE, "r") as input_file:
    rows = list(csv.reader(input_file))

#Remove the header row
header = rows.pop(0)

# Create a dictionary to store the counts of rows for each unique game number in each region
region_counts = defaultdict(lambda: defaultdict(int))

# Iterate through the rows and update the counts
for row in rows:
    game_number = row[0]
    region = row[-1]
    region_counts[game_number][region] += 1

# Open the output file and write the counts
with open(OUTPUT_FILE, "w", newline='') as output_file:
    csv_writer = csv.writer(output_file)
    csv_writer.writerow(["Game Number", "Top", "Middle", "Bottom", "Sum", "Top Ratio", "Bottom Ratio", "Cheating Percentage Realised"])
    for game_number, counts in region_counts.items():
        sum_counts = sum(counts.values())
        top_ratio = counts["top"] / sum_counts
        bottom_ratio = counts["bottom"] / sum_counts
        csv_writer.writerow([game_number, counts["top"], counts["middle"], counts["bottom"], sum_counts, top_ratio, bottom_ratio, 0])





