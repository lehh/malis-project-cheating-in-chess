# Malis Project
- File [v7.ipynb](v7.ipynb) contain the code to extract some information from pgn file (which has evaluation score and time moves) into a csv file.
- Folder [backupv7_100000game](backupv7_100000game) contains the results after we run the file [v7.ipynb](v7.ipynb) with a pgn file of 100000 games. The results contains the move of different type of game (Game type 600+0 means that the time is 10 minutes for each player. There are no increasement time between each move)
- Folder [backupv7_1million_games](backupv7_1million_games) contains the data that we run the file [v7.ipynb](v7.ipynb) with a pgn file of one million games. We only set the output to a specific type of game: 600+0.
- Folder [data](data) contains two small pgn files. One file has the evaluation score, one file does not have the evaluation score.
- Folder [ches_engine](chess_engine) contains the program stockfish (for window and linux)
- Folder [ML Elo 1000-1500 cheaters 50-100%](ML Elo 1000-1500 cheaters 50-100%) contains the code that apply machine learning method to our created data.

## Preparing the data for training:
Normally, the chess game download from internet is a pgn file (a text file). We need to use some python chess library to be able to extract the information that we want. We describe step by step to get the data for training

 1.  Download the chess game from Lichess: [https://database.lichess.org/](https://database.lichess.org/)
 We download the file '2022 - December' (about 30GB), then extract it with the instruction from Lichess in the same page. The file after extracting is a pgn file with about 200GB.
 
 2. We install a tool call pgn-extract (in linux):
 ```
 sudo apt install pgn-extract
 ```
 This tool allows us to be split the big pgn file into small one. The below command will split the file that we want into small files, each file contains 1000000 (one million) games and has name 1.pgn, 2.pgn,...
 ```
 pgn-extract -#1000000 <name of the file>
 ```
 We take one file, for example 1.pgn, and put it's path to our program ([v7.ipynb](v7.ipynb)).

 3. Run the file [v7.ipynb](v7.ipynb).

## File [v7.ipynb](v7.ipynb)
A pgn file normally only has some headers and game moves. However, it can also has some other information such as the time (show by %clk in pgn file), evaluation score (show by %eval in pgn file).

The file [v7.ipynb](v7.ipynb) will create a csv file about all move of all game with it's related information such as: game type, accuracy, player name, player elo, who play the move (white or black), the player win or loss or draw,...

With some python chess library, we can extract some information such as time, evaluation,... To caculate the accuracy, we use a formula from [https://lichess.org/page/accuracy](https://lichess.org/page/accuracy). It is calculated base on the difference between the centerpawn score (evaluation score) between two consecutive moves.

In the file, we also show a way to generate the accuracy (depend on evaluation score) using stockfish library if the pgn file does not has these evaluation score. However it is faster to extract the information from the games which have these score. Note that, according to Lichess, only 6% of the games has the evaluation score.

We run the file [v7.ipynb](v7.ipynb) with one million games (about 23GB) extracted from the downloaded file from lichess. It generated a csv file of 54 MB for training. The running time is 27 mins ![pictures/one_million_games_running_time.png](pictures/one_million_games_running_time.png)